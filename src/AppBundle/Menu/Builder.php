<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function topMenu(FactoryInterface $factory)
    {
        $menu = $factory->createItem('root');

        $menu->addChild(
          'All',
          [
            'route' => 'records',
            'label' => 'All Contacts',
            'attributes' => [
                'class' => 'display-toggle-button'
            ]
          ]);

        $menu->addChild(
          'Divider',
          [
            'label' => '|',
            'attributes' => [
                'class' => 'display-toggle-button'
            ]
          ]);

        $menu->addChild(
          'New',
          [
            'route' => 'new_records',
            'label' => 'New Contacts',
            'attributes' => [
                'class' => 'display-toggle-button'
            ]
          ]);


        return $menu;
    }
}
